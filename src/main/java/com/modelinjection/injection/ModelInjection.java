package com.modelinjection.injection;

import com.codeinjection.annotations.tool.DefaultClassInjectionParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by rramirezb on 26/12/2014.
 */
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface ModelInjection {
    boolean generatedId() default true;
    String propertyId() default "";
    String defaultPropertyIdType() default "java.util.UUID";
    boolean foreignAutoRefresh() default false;
    boolean suppressPOJOPrefixes() default false;
    DefaultClassInjectionParams classParams() default @DefaultClassInjectionParams();
}
